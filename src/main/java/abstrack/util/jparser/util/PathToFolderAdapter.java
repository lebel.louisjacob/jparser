package abstrack.util.jparser.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

final class PathToFolderAdapter implements Folder
{
    private final Path path;

    PathToFolderAdapter(final Path path)
    {
        this.path = path;
    }

    @Override
    public String getName()
    {
        return this.path.getFileName().toString();
    }

    @Override
    public Stream<Path> getContent()
    {
        try
        {
            return Files.list(this.path);
        }
        catch(final IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
