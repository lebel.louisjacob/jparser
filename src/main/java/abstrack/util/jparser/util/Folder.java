package abstrack.util.jparser.util;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface Folder extends FileSystemObject<Stream<Path>>
{
    static Folder ofPath(final Path path)
    {
        return new PathToFolderAdapter(path);
    }
}
