package abstrack.util.jparser.scan;

import abstrack.util.jparser.SyntaxException;

class NegativeScanException extends SyntaxException
{
    <Input> NegativeScanException(final Input input)
    {
        super(String.format("unexpectedly succeeded to parse\n%s", input));
    }
}
