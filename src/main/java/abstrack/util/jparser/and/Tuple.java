package abstrack.util.jparser.and;

import java.util.function.BiFunction;
import java.util.function.Function;

public interface Tuple<Left, Right>
{
    static <Left, Right> Tuple<Left, Right> of(final Left left, final Right right)
    {
        return new RegularTuple<>(left, right);
    }

    <Type> Type flatMap(BiFunction<? super Left, ? super Right, ? extends Type> function);

    default Left left()
    {
        return this.flatMap((left, right) -> left);
    }

    default Right right()
    {
        return this.flatMap((left, right) -> right);
    }

    default <NewLeft> Tuple<NewLeft, Right> mapLeft(
        final Function<? super Left, ? extends NewLeft> leftFunction)
    {
        return this.flatMap((left, right) -> Tuple.of(leftFunction.apply(left), right));
    }

    default <NewRight> Tuple<Left, NewRight> mapRight(
        final Function<? super Right, ? extends NewRight> rightFunction)
    {
        return this.flatMap((left, right) -> Tuple.of(left, rightFunction.apply(right)));
    }

    default <Type> Type flatMapLeft(
        final Function<? super Left, ? extends Type> flatLeftFunction)
    {
        return this.flatMap((left, right) -> flatLeftFunction.apply(left));
    }

    default <Type> Type flatMapRight(
        final Function<? super Right, ? extends Type> flatRightFunction)
    {
        return this.flatMap((left, right) -> flatRightFunction.apply(right));
    }
}
