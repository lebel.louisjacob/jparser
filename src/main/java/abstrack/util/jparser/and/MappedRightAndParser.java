package abstrack.util.jparser.and;

import java.util.function.Function;

final class MappedRightAndParser<Input, LeftOutput, RightOutput, NewRightOutput>
    implements AndParser<Input, LeftOutput, NewRightOutput>
{
    private final AndParser<? super Input, LeftOutput, ? extends RightOutput> andParser;
    private final Function<? super RightOutput, ? extends NewRightOutput> rightFunction;

    MappedRightAndParser(
        final AndParser<? super Input, LeftOutput, ? extends RightOutput> andParser,
        final Function<? super RightOutput, ? extends NewRightOutput> rightFunction)
    {
        this.andParser = andParser;
        this.rightFunction = rightFunction;
    }

    @Override
    public Tuple<LeftOutput, NewRightOutput> apply(final Input input)
    {
        final var result = this.andParser.apply(input);
        return result.mapRight(this.rightFunction);
    }
}
