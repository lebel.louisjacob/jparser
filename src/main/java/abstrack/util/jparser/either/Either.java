package abstrack.util.jparser.either;

import java.util.Optional;
import java.util.function.Function;

public interface Either<Left, Right>
{
    static <Left, Right> Either<Left, Right> leftOf(final Left left)
    {
        return new LeftEither<>(left);
    }

    static <Left, Right> Either<Left, Right> rightOf(final Right right)
    {
        return new RightEither<>(right);
    }

    Optional<Left> left();

    Optional<Right> right();

    <Output> Output map(
        Function<? super Left, ? extends Output> leftFunction,
        Function<? super Right, ? extends Output> rightFunction);
}
