package abstrack.util.jparser.either;

import java.util.Optional;
import java.util.function.Function;

final class LeftEither<Left, Right> implements Either<Left, Right>
{
    private final Left left;

    LeftEither(final Left left)
    {
        this.left = left;
    }

    @Override
    public Optional<Left> left()
    {
        return Optional.of(this.left);
    }

    @Override
    public Optional<Right> right()
    {
        return Optional.empty();
    }

    @Override
    public <Output> Output map(
        final Function<? super Left, ? extends Output> leftFunction,
        final Function<? super Right, ? extends Output> rightFunction)
    {
        return leftFunction.apply(this.left);
    }
}
