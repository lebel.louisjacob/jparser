package abstrack.util.jparser.either;

import abstrack.util.jparser.Parser;
import abstrack.util.jparser.optional.OptionalParser;

import java.util.function.Function;

@FunctionalInterface
public interface EitherParser<Input, LeftOutput, RightOutput> extends Parser<Input, Either<LeftOutput, RightOutput>>
{
    static <Input, LeftOutput, RightOutput> EitherParser<Input, LeftOutput, RightOutput> of(
        final Function<? super Input, ? extends LeftOutput> leftParser,
        final Function<? super Input, ? extends RightOutput> rightParser)
    {
        return new RegularEitherParser<>(leftParser, rightParser);
    }

    default OptionalParser<Input, LeftOutput> tryLeft()
    {
        return this
            .map(either -> either
                .left()
                .orElseThrow(EmptyEitherSyntaxException::new))
            .optional();
    }

    default OptionalParser<Input, RightOutput> tryRight()
    {
        return this
            .map(either -> either
                .right()
                .orElseThrow(EmptyEitherSyntaxException::new))
            .optional();
    }

    default <Output> Parser<Input, Output> map(
        final Function<? super LeftOutput, ? extends Output> leftFunction,
        final Function<? super RightOutput, ? extends Output> rightFunction)
    {
        return this.map(either -> either.map(leftFunction, rightFunction));
    }
}
