package abstrack.util.jparser.either;

import abstrack.util.jparser.SyntaxException;

class EmptyEitherSyntaxException extends SyntaxException
{
    EmptyEitherSyntaxException()
    {
        super("");
    }
}
