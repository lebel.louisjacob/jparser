package abstrack.util.jparser;

public class SyntaxException extends RuntimeException
{
    public SyntaxException(final Object message)
    {
        super(message.toString());
    }
}
