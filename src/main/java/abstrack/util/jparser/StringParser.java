package abstrack.util.jparser;

import compiler.parser.split.SplitParser;

import java.util.regex.Pattern;

public final class StringParser
{
    private StringParser()
    {}

    public static SplitParser<String, String> anyCharacter()
    {
        return StringParser.regexOf(".");
    }

    public static SplitParser<String, String> literalOf(final String literal)
    {
        return StringParser.regexOf(Pattern.quote(literal));
    }

    public static SplitParser<String, String> regexOf(final String regex)
    {
        return new RegexParser(regex);
    }
}
